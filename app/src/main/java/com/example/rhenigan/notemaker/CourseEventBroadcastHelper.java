package com.example.rhenigan.notemaker;

import android.content.Context;
import android.content.Intent;

/**
 * Created by henig on 8/26/2018.
 * ------------------------------
 */
public class CourseEventBroadcastHelper {

    public static final String ACTION_COURSE_EVENT = "com.example.rhenigan.notemaker.action.COURSE_EVENT";
    public static final String EXTRA_COURSE_ID = "com.example.rhenigan.notemaker.extra.COURSE_ID";
    public static final String EXTRA_COURSE_MESSAGE = "com.example.rhenigan.notemaker.extra.COURSE_MESSAGE";

    public static void sendEventBroadcast(Context context, String courseId, String message) {
        Intent intent = new Intent(ACTION_COURSE_EVENT);
        intent.putExtra(EXTRA_COURSE_ID, courseId);
        intent.putExtra(EXTRA_COURSE_MESSAGE, message);

        context.sendBroadcast(intent);
    }


}
